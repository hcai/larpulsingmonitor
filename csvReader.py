import matplotlib.pyplot as plt
import csv
import os,sys
from pathlib import Path

fig, axs = plt.subplots(figsize=(12, 4))
outputDir=str(sys.argv[1])
plotsDir=outputDir+"/plots"
Path(plotsDir).mkdir(parents=True, exist_ok=True)

def makePlot(TT,cell):
    DataPath=outputDir+"/"+TT+"/"+cell
    for row in csv.reader(open(DataPath+"_CH1vert.csv", 'r')):
        fVert = [float(x) for x in row]
    for row in csv.reader(open(DataPath+"_horiz.csv", 'r')):
        fHoriz = [float(x) for x in row]
    axs.plot(fHoriz,fVert)
    fig.xlabel('time [t]')
    fig.ylabel('ADC [V]')
    fig.title('%s/%s' % (TT,cell))
    Path(plotsDir+"/"+TT).mkdir(parents=True, exist_ok=True)
    fig.savefig('%s.png' % cell,dpi=fig.dpi)

def getTTList(Dir):
    List=[]
    for r,d,f in os.walk(Dir):
        for dirs in d:
            if dirs.find("0x") != -1:
                List.append(dirs)
    return List

def getCellList(Dir):
    List=[]
    for r,d,f in os.walk(Dir):
        for files in f:
            if files.find("_CH1vert.csv") !=-1:
                List.append(files[0:files.find("_")])
    return List

TTList=getTTList(outputDir)
plotsDict={}
for TT in TTList:
    plotsDict[TT]=getCellList(outputDir+"/"+TT)
    for cell in plotsDict[TT]:
        makePlot(TT,cell)
