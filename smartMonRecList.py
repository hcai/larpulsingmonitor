#script to generate a TT list for monRecChan for an interesting TTid
#author: Huacheng Cai

import os, sys

DET = int(sys.argv[1])
SIDE = str(sys.argv[2])
TT = str(sys.argv[3])

DETmaplist = ['EMB','EMEC','HECFCAL','HECFCAL']
ACmap = {1:'A','1':'A','A':'A',-1:'C','-1':'C','C-':'C'}

TT_LIST_DUMMY_BASE={"EMBA":  0x100,
                    "EMBC":  0x1130,
                    "EMECA": 0x2110,
                    "EMECC": 0x1100,
                    "HECA":  0x4190,
                    "HECC":  0x5170,
                    "HECFCALA": 0x4100,
                    "HECFCALC": 0x5100}
#                            0x5100000
#print(hex(TT_LIST_DUMMY_BASE["FCALC"]*pow(16,3)))

#produce dummy list

detector=DETmaplist[DET]+ACmap[SIDE]

TT_LIST_DUMMY=[]
for x in range(4):
    for y in range(4):
        TT_LIST_DUMMY.append(hex(TT_LIST_DUMMY_BASE[detector]*pow(16,3)+x*pow(16,2)+y))

#check if target TT is in the list
if TT in TT_LIST_DUMMY:
    #swap
    index=TT_LIST_DUMMY.index(TT)
    tmp=TT_LIST_DUMMY[0]
    TT_LIST_DUMMY[0]=TT
    TT_LIST_DUMMY[index]=tmp
else:
    TT_LIST_DUMMY[0]=TT
    
#preparing output
output_str='-c '+detector+' -p '+TT
for tt in TT_LIST_DUMMY:
    if tt==TT:
        continue
    else:
        output_str+=','
        output_str+=tt

print(output_str)
