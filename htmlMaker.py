import pandas as pd
import plotly.graph_objects as go
import os,sys,csv
from pathlib import Path


outputDir=str(sys.argv[1])
htmlDir=outputDir+"/html"
Path(htmlDir).mkdir(parents=True, exist_ok=True)

#DataPathList=['0x5100b01/0x3a330f00','0x5100b02/0x3a334f00','0x5100502/0x3a30cf00']

def addPlot(DataPath, fig):
    for row in csv.reader(open(DataPath+"_CH1vert.csv", 'r')):
        fVert = [float(x) for x in row]
    for row in csv.reader(open(DataPath+"_horiz.csv", 'r')):
        fHoriz = [float(x) for x in row]
    fig.add_trace(go.Scatter(x = fHoriz[len(fHoriz)*45//100:len(fHoriz)*7//10], y = fVert[len(fVert)*45//100:len(fVert)*7//10],
                             mode='lines',name=DataPath))

def makePlot(TT):
    fig = go.Figure()
    fig.update_layout(title='Pluse Shape',
                      plot_bgcolor='rgb(230, 230,230)',
                      showlegend=True)
    for cell in plotsDict[TT]:
        dataPath=outputDir+"/"+TT+"/"+cell
        addPlot(dataPath,fig)
    fig.write_html("%s/%s.html" % (htmlDir, TT))

def getTTList(Dir):
    List=[]
    for r,d,f in os.walk(Dir):
        for dirs in d:
            if dirs.find("0x") != -1:
                List.append(dirs)
    return List

def getCellList(Dir):
    List=[]
    for r,d,f in os.walk(Dir):
        for files in f:
            if files.find("_CH1vert.csv") !=-1:
                List.append(files[0:files.find("_")])
    return List

TTList=getTTList(outputDir)
plotsDict={}
for TT in TTList:
    plotsDict[TT]=getCellList(outputDir+"/"+TT)
    makePlot(TT)
