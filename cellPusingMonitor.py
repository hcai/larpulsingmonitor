#!/usr/bin/env python
#Author: Huacheng Cai

import sqlite3
import math
import os, sys
import subprocess
DB = '/det/lar/project/databases/LArId.db'
SAPulsingDir=os.getenv("SAPULSING_DIR")
scopemonDir=os.getenv("SCOPEMON_DIR")

DETmaplist = ['EMB','EMEC','HECFCAL','HECFCAL']
ACmap = {1:'A+','1':'A+','A+':'A+',-1:'C-','-1':'C-','C-':'C-'}

if not os.path.exists(DB):
  print('DB ' + DB + ' not found. Aborting!')
  sys.exit(0)

try:
  conn = sqlite3.connect(DB)
except:
  print('Could not connect to LArId database. Aborting!')
  sys.exit(0)

conn.row_factory  = sqlite3.Row
c = conn.cursor()

###consistency check

def getCALIB_from_ONLID(DET,AC,ONL_IDs):
   #print('Detector='+DETmaplist[DET])
   #print('Side='+ACmap[AC][0])

   cells_list = ONL_IDs.split(',')
   rows = []
   for cell in cells_list:
      var = (DET, AC, eval(cell), )
      #var=(eval(cell))
      try:
         c.execute('SELECT TT_COOL_ID, FTNAME, TtcRx_Address, ONL_ID FROM LARID WHERE DET = ? AND AC = ? AND ONL_ID = ?', var)
         #c.execute('SELECT TT_COOL_ID, FTNAME, TtcRx_Address, ONL_ID FROM LARID WHERE ONL_ID = ?', var)
      except:
         print("SQL query failed")
      this_row = c.fetchall()
      rows.append(this_row[0])
   return rows

DET = int(sys.argv[1])
SIDE = str(sys.argv[2])
ONL_IDS = str(sys.argv[3])
flag = int(sys.argv[4])

#Initalize setup for Standalone pulsing..
#subprocess.Popen("SCOPEMON_DIR=/det/lar/project/usersarea/hcai/scopemon", stdout=subprocess.PIPE)

#Loop over cells..
rows=getCALIB_from_ONLID(DET, SIDE, ONL_IDS)
row=rows[0]
TT=str(hex(row[0])) #0
FTNAME=str(row[1]) #1
TTCRX=str(hex(row[2])) #2
ONL_ID=hex(row[3]) #3
SCOPENAME="TCPIP::PC-LAR-SCOPE-19%s" % ACmap[SIDE][0] #4

if flag==0:
  print(TT)
if flag==1:
  print(FTNAME)
if flag==2:
  print(TTCRX)
if flag==3:
  print(ONL_ID)
if flag==4:
  print(SCOPENAME)


  #send pulsing
  #bashCmd=["cd", SAPulsingDir]
  #print(bashCmd)  
  #subprocess.Popen(bashCmd, stdout=subprocess.PIPE)
#  bashCmd=["sh","etaphiCalibCommand.sh", 
#           "-d=%s" % str(DET),
#           "-S=%d" % SIDE,
#           "-D=%d" % 6000,
#           "-T=%s" % str(TTCrx),
#           "-O=%s" % str(ONL_ID),
#           "-t=%s" % str(2.e-4),
#           "-N=%d" % 100000,
#           "-A"]
#  bashCmd=["sh",
#           "%s/etaphiCalibCommand.sh" % SAPulsingDir,
#           "-d", str(DET),
#           "-S=%d" % SIDE,
#           "-D", str(6000),
#           "-T", str(TTCrx),
#           "-O", str(ONL_ID),
#           "-t", str(2.e-4),
#           "-N", str(100000),
#           "-A"]
# % (SAPulsingDir, DET, SIDE,str(TTCrx),str(ONL_IDS[0]))]
#  print(bashCmd)
#  process1=subprocess.run(bashCmd, )
#  bashCmd=["ssh","pc-l1c-mon-06.cern.ch"," python %s/scopeCLI_acquire.py 1 %s/%s -L TCPIP::PC-LAR-SCOPE-19%s -i" % (scopemonDir, outputDir,str(TT),ACmap[SIDE][0])]
#  print(bashCmd)
#  process2=subprocess.Popen(bashCmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  
  
