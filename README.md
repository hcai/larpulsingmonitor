# LArPulsingMonitor

LArPulsingMonitor is a wrapper for sending pulses to a given list of LAr cells and record the shape of the pulse. It is a combination of the [StandAlonePulsingTool](https://atlasop.cern.ch/twiki/bin/view/LAr/SPACPulsing#Step_by_Step_Procedure_for_Conne), [L1CReceiverTool](https://gitlab.cern.ch/hcai/receiverTools/) and [scopemon](https://gitlab.cern.ch/hcai/scopemon/).

## Setup

Running on l1calo monitoring PC `pc-l1c-mon-06`. Using the general LAr and L1Calo config

```bash
sod #LAr
source /det/l1calo/scripts/setup.sh #L1Cao
```

## Usage

```
./cellPusingMonitor_l1cmon.sh -D <0:EMB, 1:EMEC, 2:HEC, 3:FCAL> -S <1:A, -1:C> -L <list of cells> -T <cell list txt file> -O <input dir>
```
The expected shape of the pulses can be checked from [LArIdTranslator](https://atlas-larmon.cern.ch/LArIdtranslator/), by clicking the link of the cell online ID.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
