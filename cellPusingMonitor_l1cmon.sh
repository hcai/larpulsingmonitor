
#!/bin/bash

export SCOPEMON_DIR=/det/lar/project/usersarea/hcai/scopemon
export OUTPUT_DIR=$PWD/output
export SAPULSING_DIR=/det/lar/project/LargOnline/patches/921.45.00/StandalonePulsing/v1r6/share/bin
#export MONRECCHANS=/det/l1calo/releases/tdaq-09-02-01/rxgain/rxgain-03-00-08/installed/x86_64-centos7-gcc8-opt/bin/monRecChans


ln -fs $SAPULSING_DIR/etaphiCalibCommand.sh etaphiCalibCommand.sh
ln -fs $SAPULSING_DIR/disable_ssws.sh disable_ssws.sh


while getopts "D:S:L:T:O:h" opt; do
    case $opt in
        D)
            echo 'det: '$OPTARG'' 
            DET=$OPTARG;;
        S)
            echo 'side: '$OPTARG''
            SIDE=$OPTARG;;
	L)
	    echo 'cell: '$OPTARG''
	    IFS=, read -a CellList <<< "$OPTARG" ;;
	T)
	    echo 'cell list file:'$OPTARG''
	    CellListFile=$OPTARG
	    CellList=()
	    while IFS= read -r line; do
		CellList+=("$line")
	    done < $CellListFile;;
	O)
	    echo 'output dir:'$OPTARG''
	    OUTPUT_DIR=$OPTARG;;
        h)
            echo '''
./cellPusingMonitor_l1cmon.sh -D <0:EMB, 1:EMEC, 2:HEC, 3:FCAL> -S <1:A, -1:C> -L <list of cells> -T <cell list txt file> -O <input dir>'''
            exit 1;;
    esac
done

#DET=3
#SIDE=-1
#declare -a CellList=(0x3a308000 0x3a309000 0x3a30a000 0x3a30b100)

#check if output dir exists
[ ! -d "$OUTPUT_DIR" ] && echo "ERROR:: Output dir $OUTPUT_DIR does NOT exists."

for cell in ${CellList[@]}; do
    echo $cell

    echo python cellPusingMonitor.py $DET $SIDE $cell
    TT=$(python cellPusingMonitor.py $DET $SIDE $cell 0 2>&1)
    FTNAME=$(python cellPusingMonitor.py $DET $SIDE $cell 1 2>&1)
    TTCRX=$(python cellPusingMonitor.py $DET $SIDE $cell 2 2>&1)
    ONL_ID=$(python cellPusingMonitor.py $DET $SIDE $cell 3 2>&1)
    SCOPENAME=$(python cellPusingMonitor.py $DET $SIDE $cell 4 2>&1)

    #configure monitoring board
    smartOutput=$(python smartMonRecList.py $DET $SIDE $TT 2>&1)
    command_to_config_mon="monRecChans $smartOutput"
    echo $command_to_config_mon
    eval $command_to_config_mon
    
    #run the SA pulsing tool
    #disable SSWs
    command_to_disable_ssw="./disable_ssws.sh -A -d $DET -S $SIDE -T $TTCRX"
    echo $command_to_disable_ssw
    eval $command_to_disable_ssw
    #send pulse
    command_to_send_pulse="./etaphiCalibCommand.sh -d $DET -S $SIDE -D 3000 -T $TTCRX -O $cell -t 2.e-4 -N 100000 -A"
    echo $command_to_send_pulse
    eval $command_to_send_pulse &
    sleep 15

    #check if the TT is okay...
    if [[ "$TT" != "0x"* ]]; then
	echo "ERROR:: illeagle TT found: <$TT>, please check your cell online ID!"
	continue
    fi
    [ ! -d "$OUTPUT_DIR/$TT" ] && mkdir $OUTPUT_DIR/$TT
    command_to_save_scope="ssh ${USER}@pc-l1c-mon-05.cern.ch python $SCOPEMON_DIR/scopeCLI_acquire.py 1 $OUTPUT_DIR/$TT/$cell -L $SCOPENAME -i --save-only"
    echo $command_to_save_scope
    eval $command_to_save_scope &
    sleep 25

done
